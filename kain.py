def standardize_names(character_name):
    output = character_name.strip()
    copy = ""
    for letra in output:
        copy += [letra if letra != " " else "_"][0]

    return copy

# EXEMPLO 1:
hero_standardized = standardize_names(" Batman     ")
print(hero_standardized)

# > Batman

# EXEMPLO 2
hero_standardized = standardize_names("      Super Man")
print(hero_standardized)

# > Super-Man

def standardize_title(title):
    return ' '.join(s[:1].upper() + s[1:] for s in title.split(' '))

# EXEMPLO 1

title = standardize_title("cinco quartos de laranja")

print(title)

# > Cinco Quartos De Laranja

def standardize_text(text):
    text = text.split(". ")
    # text = [frase for frase in text]
    text = [frase.capitalize() for frase in text]
    text = '. '.join(text)
    
    return text

# EXEMPLO 1

text = """a famosa atriz Constance Rattigan recebe uma encomenda desagradável: uma lista com números de
telefone de pessoas que morreram recentemente. é uma coisa assustadora, considerando que os nomes das
poucas pessoas vivas presentes na lista estão assinalados em vermelho com
uma cruz. O da própria Constance é um deles."""

normalized_text = standardize_text(text)
print(normalized_text)

# > A famosa atriz Constance Rattigan recebe uma encomenda desagradável: uma 
# lista com números de telefone de pessoas que morreram recentemente. É uma 
# coisa assustadora, considerando que os nomes das poucas pessoas vivas presentes
# na lista estão assinalados em vermelho com uma cruz. O da própria Constance é um deles.

def title_creator(text):
    hifem = "--------------------"
    text = text.capitalize()
    output = hifem + text + hifem
    return output

text = "pense num deserto"
title = title_creator(text)

print(title)

def text_merge(text_of_blog_a, text_of_blog_b):
    text_a = text_of_blog_a.strip()
    text_b = text_of_blog_b.strip()
    text_a = text_a[:-1]
    text_final = text_a + '\n' +text_b
    text_final = text_final.split(' ')
    text_final = [palavra for palavra in text_final if palavra != "" ]
    text_final = ' '.join(text_final)
    text_final = standardize_text(text_final)   
    return text_final


text_of_blog_a = """
na Londres do pós-guerra, a escritora     Juliet tenta encontrar uma   trama para seu novo livro. ela 
recebe ajuda por meio de uma     carta de um      desconhecido, um nativo da ilha de Guernsey, 
em cujas mãos havia chegado um livro    que há tempos tinha pertencido    a Juliet.
"""
text_of_blog_b = """
O romance "Cinco Quartos de Laranja" é como   um vinho intenso e delicado.    usando metáforas
culinárias, personagens peculiares   e acontecimentos sobrenaturais,      Harris cria uma história
complexa e      bela ao mesmo tempo.
"""

merged_text = text_merge(text_of_blog_a, text_of_blog_b)

print(merged_text)